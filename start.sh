#!/bin/bash

source venv/bin/activate

pip install --upgrade -r requirements.txt
./manage.py migrate
./manage.py collectstatic --no-input

gunicorn -b 0.0.0.0:8080 userportal.wsgi

