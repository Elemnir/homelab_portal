=======================
 Howard Homelab Portal
=======================

A testbed system for software development, playing with ideas, and providing some basic services for Homelab users.

-------
 Setup
-------

This is a Django webapp and should be deployed using the standard Python3/VirtualEnv/Gunicorn stack for deployment.

The following environment variables should be set when starting up the service, and contain secrets used in the settings file.

::

    DJANGO_DEBUG=''                 # Optional, but if set to 'True' it enables Django debugging
    DJANGO_ALLOWED_HOST=''          # A large random text string for seeding Django
    DJANGO_SECRET_KEY=''            # A large random text string for seeding Django
    DJANGO_DB_HOSTNAME=''           # Name of the PostgreSQL DB Host
    DJANGO_DB_USERNAME=''           # Username to use when connecting to the DB
    DJANGO_DB_DATABASE=''           # Name of the database to connect too
    DJANGO_DB_PASSWORD=''           # Password to use when connecting to the DB
    DJANGO_DB_PASSWORD=''           # Password to use when connecting to the DB
    DJANGO_DRAMATIQ_BROKER_URL=''   # Address of the Redis server for Dramatiq ('redis://example.com:6379')
    DJANGO_DUO_SKEY=''              # A large random text string to be used as a secret key by Duo
    DJANGO_DUO_AKEY=''              # The application key for a Duo application
    DJANGO_DUO_IKEY=''              # The integration key for a Duo application
    DJANGO_DUO_HOST=''              # The hostname for a Duo application
    DJANGO_EMAIL_USERNAME=''        # Email address to use when sending emails
    DJANGO_EMAIL_PASSWORD=''        # Password to use when sending emails
    DJANGO_EMAIL_HOST=''            # The Email server's hostname
    DJANGO_LDAP_SERVER_URI=''       # Address of the LDAP server ('ldap://example.com')
    DJANGO_LDAP_BASE=''             # Base domain to use for LDAP searches ('dc=example,dc=com')
    DJANGO_LDAP_BIND_PASSWORD=''    # Password to use when binding to the LDAP Server

The webapp also relies on a number of supplementary services which should be configured and available:

 - PostgreSQL server - For the Django database
 - Redis server - For the Dramatiq task queue to use as a transport layer
 - LDAP server - Used for identity managment on the portal
 - Nginx - Not necessarily required, but used as a frontend to the Gunicorn server


