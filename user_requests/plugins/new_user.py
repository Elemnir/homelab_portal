from django.contrib.auth.password_validation    import validate_password
from django.db                                  import models
from django.shortcuts                           import redirect
from django.urls                                import path, reverse
from django.views.generic                       import TemplateView
from django.views.generic.edit                  import FormView

from admin_toolbelt.tasks   import set_actor_queue, create_path

from openacct.models        import User as ProjectUser
from user_requests.models   import Request, RequestType, request_log
from user_requests.views    import RequestUpdateView, check_admin
from userportal.utils       import BootstrapModelForm, get_ldap_client


class NewUserRequest(RequestType):
    request_description = 'Request a new account.'
    form_view_name = 'user_requests:new_user_info'
    update_view_name = 'user_requests:update_new_user'
    widget_template_path = 'requests/new_user/review_widget.html'

    name        = models.CharField(max_length=128)
    email       = models.EmailField(max_length=256)
    username    = models.CharField(max_length=8)
    password    = models.CharField(max_length=1024)
    quota       = models.CharField(max_length=8, blank=True, default='5G')

    def on_approved(self):
        with get_ldap_client() as lc:
            # Create the user in LDAP
            lc.create_user(
                self.username, lc.next_user_uid(), self.name, self.email, self.password, 
                'users', hash_password=False
            )
            entry = lc.search_user(self.username)
            
            # Add the user to the openacct system
            ProjectUser.objects.create(name=self.username, realname=self.name)

            # Create the user's home directory
            set_actor_queue(create_path, 'homedirs')
            create_path.send(
                '/ssd-pool/user/{}'.format(entry['uid'][0]), entry['uid'][0], 
                entry['gidNumber'][0], usage_quota=self.quota, copy_files=[
                    ('/etc/skel/.bashrc',       '.bashrc'),
                    ('/etc/skel/.bash_profile', '.bash_profile'),
                    ('/etc/skel/.bash_logout',  '.bash_logout'),
                ]
            )


class NewUserRequestForm(BootstrapModelForm):
    class Meta:
        model = NewUserRequest
        fields = ['name', 'email', 'username', 'password', 'quota']

    def clean(self):
        cd = super().clean()
        with get_ldap_client() as lc:
            if lc.search_user(cd.get('username')) is not None:
                self.add_error('username', 'That username is already in use.')
            passwd = cd.get('password', '')
            validate_password(passwd) # Raises a ValidationError if not valid
            cd['password'] = lc.hash_password(passwd).decode()
            return cd


class NewUserRequestUpdateView(RequestUpdateView):
    model = NewUserRequest
    modelform = NewUserRequestForm
    fields = ['name', 'email', 'username', 'quota']


class NewUserRequestFormView(FormView):
    template_name = 'requests/new_user/form.html'
    form_class = NewUserRequestForm

    def form_valid(self, form):
        req = form.save(commit=False)
        req.request = Request.objects.create(
            confirmed = True, # Auto-confirm new user requests
            description = 'New User: {0}'.format(req.username),
            requester = req.username,
            status = 'NEW'
        )
        req.request.save()
        req.save()
        return redirect(reverse('user_requests:new_user_conclusion'))


class NewUserConclusionView(TemplateView):
    template_name = 'requests/new_user/conclusion.html'

request_types = [
    NewUserRequest,
]

urlpatterns = [
    path('new_user/info/', NewUserRequestFormView.as_view(), name='new_user_info'),
    path('update/new_user/<int:pk>/', NewUserRequestUpdateView.as_view(), name='update_new_user'),
    path('new_user/conclusion/', NewUserConclusionView.as_view(), name='new_user_conclusion'),
]

