from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions     import ObjectDoesNotExist, ValidationError
from django.db                  import models
from django.shortcuts           import redirect
from django.urls                import path, reverse
from django.views.generic.edit  import FormView

from openacct.models        import Project, User as ProjectUser 
from user_requests.models   import Request, RequestType, request_log
from user_requests.views    import RequestUpdateView, check_admin
from userportal.utils       import BootstrapModelForm, get_ldap_client

class ProjectAccessRequest(RequestType):
    request_description = 'Request access to an existing project.'
    form_view_name = 'user_requests:project_access_info'
    update_view_name = 'user_requests:update_project_access'
    widget_template_path = 'requests/project_access/review_widget.html'

    project_name = models.CharField(max_length=32)
    project_pi = models.CharField(max_length=128, blank=True)
    project_desc = models.CharField(max_length=1024, blank=True)

    def on_approved(self):
        user = ProjectUser.objects.get(name=self.request.requester)
        proj = Project.objects.get(name=self.project_name)
        user.projects.add(proj)
        with get_ldap_client() as lc:
            lc.add_user_to_group(user.name, proj.ldap_group)


class ProjectAccessRequestForm(BootstrapModelForm):
    class Meta:
        model = ProjectAccessRequest
        fields = ['project_name']

    def clean(self):
        cd = super().clean()
        project_name = cd['project_name'] = cd.get('project_name', '')
        try:
            proj = Project.objects.get(name=project_name, active=True)
            cd['project_pi'], cd['project_desc'] = proj.pi.realname, proj.description
            return cd
        except ObjectDoesNotExist:
            raise ValidationError("A project with that ID does not exist or isn't active.")

    def save(self, commit=True):
        self.instance.project_pi = self.cleaned_data['project_pi']
        self.instance.project_desc = self.cleaned_data['project_desc']
        return super().save(commit=commit)


class ProjectAccessRequestUpdateView(RequestUpdateView):
    model = ProjectAccessRequest
    modelform = ProjectAccessRequestForm
    fields = ['project_name']


class ProjectAccessRequestFormView(LoginRequiredMixin, FormView):
    template_name = 'requests/project_access/form.html'
    form_class = ProjectAccessRequestForm

    def form_valid(self, form):
        req = form.save(commit=False)
        req.request = Request.make_from_http_request(self.request,
            description = 'Add User to Project: {0}'.format(req.project_name),
        )
        req.request.save()
        req.save()
        return redirect(req.request.review_url())


request_types = [
    ProjectAccessRequest,
]

urlpatterns = [
    path('project_access/info/', ProjectAccessRequestFormView.as_view(), name='project_access_info'),
    path('update/project_access/<int:pk>/', ProjectAccessRequestUpdateView.as_view(), name='update_project_access'),
]
