from django.contrib.auth.mixins import LoginRequiredMixin
from django.db                  import models
from django.shortcuts           import redirect
from django.urls                import path, reverse
from django.views.generic.edit  import FormView

from openacct.models        import Project
from user_requests.models   import Request, RequestType, request_log
from user_requests.views    import RequestUpdateView, check_admin
from userportal.utils       import BootstrapModelForm, get_ldap_client


class NewMachineRequest(RequestType):
    request_description = 'Request a new virtual machine.'
    form_view_name = 'user_requests:new_vm_info'
    update_view_name = 'user_requests:update_new_vm'
    widget_template_path = 'requests/new_vm/review_widget.html'

    name    = models.CharField(max_length=1024)
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE, limit_choices_to={'active': True}
    )
    cores   = models.IntegerField(default=1)
    memgb   = models.IntegerField(default=2)
    diskgb  = models.IntegerField(default=4)

    description = models.TextField()
    packages    = models.TextField()
    privileges  = models.TextField()

    def on_approved(self):
        pass

class NewMachineRequestForm(BootstrapModelForm):
    class Meta:
        model = NewMachineRequest
        fields = [
            'name', 'project', 'cores', 'memgb', 'diskgb', 
            'description', 'packages', 'privileges'
        ]


class NewMachineRequestUpdateView(RequestUpdateView):
    model = NewMachineRequest
    modelform = NewMachineRequestForm
    fields = [
        'name', 'cores', 'memgb', 'diskgb', 'description', 'packages', 'privileges'
    ]



class NewMachineRequestFormView(LoginRequiredMixin, FormView):
    template_name = 'requests/new_vm/form.html'
    form_class = NewMachineRequestForm

    def form_valid(self, form):
        req = form.save(commit=False)
        req.request = Request.objects.create(
            description = 'New Virtual Machine: {0}'.format(req.name),
            requester = self.request.user.username,
            status = 'NEW'
        )
        req.request.save()
        req.save()
        return redirect(req.request.review_url())


request_types = [
    NewMachineRequest,
]

urlpatterns = [
    path('new_vm/info/', NewMachineRequestFormView.as_view(), name='new_vm_info'),
    path('update/new_vm/<int:pk>/', NewMachineRequestUpdateView.as_view(), name='update_new_vm'),
]
