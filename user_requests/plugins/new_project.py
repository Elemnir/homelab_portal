from django.contrib.auth.mixins import LoginRequiredMixin
from django.db                  import models
from django.shortcuts           import redirect
from django.urls                import path, reverse
from django.views.generic.edit  import FormView

from openacct.models        import Project, User as ProjectUser
from user_requests.models   import Request, RequestType, request_log
from user_requests.views    import RequestUpdateView, check_admin
from userportal.utils       import BootstrapModelForm, get_ldap_client


class Affiliation(models.Model):
    description = models.CharField(max_length=128)
    code        = models.CharField(max_length=16)
    indexlength = models.IntegerField(default=2, blank=True)
    available   = models.BooleanField(default=True, blank=True)

    def __str__(self):
        return self.description


class NewProjectRequest(RequestType):
    request_description = 'Request a new project.'
    form_view_name = 'user_requests:new_project_info'
    update_view_name = 'user_requests:update_new_project'
    widget_template_path = 'requests/new_project/review_widget.html'

    title       = models.CharField(max_length=1024)
    affiliation = models.ForeignKey(
        Affiliation, on_delete=models.CASCADE, limit_choices_to={'available': True}
    )
    description = models.TextField()
    document    = models.FileField(upload_to="user_requests/new_project", blank=True)

    def on_approved(self):
        next_idx = Project.next_index(self.affiliation.code)
        project_name = (
            self.affiliation.code + str(next_idx).zfill(self.affiliation.indexlength)
        )

        with get_ldap_client() as lc:
            lc.create_group(
                project_name.lower(), lc.next_group_gid(), 
                members=[self.request.requester], 
                description=self.title
            )

            pi = ProjectUser.objects.get(name=self.request.requester)
            proj = Project.objects.create(
                name=project_name.upper(), ldap_group=project_name.lower(), 
                pi=pi, description=self.title
            )
            pi.projects.add(proj)
            pi.save()


class NewProjectRequestForm(BootstrapModelForm):
    class Meta:
        model = NewProjectRequest
        fields = ['title', 'affiliation', 'description', 'document']


class NewProjectRequestUpdateView(RequestUpdateView):
    model = NewProjectRequest
    modelform = NewProjectRequestForm
    fields = ['title', 'affiliation', 'description']


class NewProjectRequestFormView(LoginRequiredMixin, FormView):
    template_name = 'requests/new_project/form.html'
    form_class = NewProjectRequestForm

    def form_valid(self, form):
        req = form.save(commit=False)
        req.request = Request.objects.create(
            description = 'New Project: {0}'.format(req.title),
            requester = self.request.user.username,
            status = 'NEW'
        )
        req.request.save()
        req.save()
        return redirect(req.request.review_url())


request_types = [
    NewProjectRequest,
]

urlpatterns = [
    path('new_project/info/', NewProjectRequestFormView.as_view(), name='new_project_info'),
    path('update/new_project/<int:pk>/', NewProjectRequestUpdateView.as_view(), name='update_new_project'),
]
