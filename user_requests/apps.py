from django.apps import AppConfig


class UserRequestsConfig(AppConfig):
    name = 'user_requests'
    label = 'user_requests'
    verbose_name = 'User Requests'
