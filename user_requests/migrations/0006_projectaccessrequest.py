# Generated by Django 2.2.7 on 2020-03-06 02:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('user_requests', '0005_affiliation_newprojectrequest'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProjectAccessRequest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('project_name', models.CharField(max_length=32)),
                ('project_pi', models.CharField(blank=True, max_length=128)),
                ('project_desc', models.CharField(blank=True, max_length=1024)),
                ('request', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user_requests.Request')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
