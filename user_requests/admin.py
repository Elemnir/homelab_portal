from django.contrib import admin

from .models    import EmailTemplate, Request


class EmailTemplateAdmin(admin.ModelAdmin):
    pass
admin.site.register(EmailTemplate, EmailTemplateAdmin)


class RequestAdmin(admin.ModelAdmin):
    pass
admin.site.register(Request, RequestAdmin)
