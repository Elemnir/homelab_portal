from django.conf                    import settings
from django.contrib.auth.mixins     import LoginRequiredMixin, UserPassesTestMixin
from django.http                    import (HttpResponseBadRequest,
                                            HttpResponseForbidden)
from django.shortcuts               import get_object_or_404, redirect, render
from django.urls                    import reverse
from django.utils.module_loading    import import_string
from django.views.generic           import View, ListView, TemplateView
from django.views.generic.edit      import UpdateView

from .models import EmailTemplate, Request, RequestLogEntry, RequestType, request_log


CHECK_ADMIN_VALUE = getattr(settings, 'USER_REQUESTS_CHECK_ADMIN', lambda u: u.is_staff)
check_admin = (
    CHECK_ADMIN_VALUE if callable(CHECK_ADMIN_VALUE) else import_string(CHECK_ADMIN_VALUE)
)


class AvailableRequestsView(LoginRequiredMixin, View):
    """Display the currently available request types"""
    def get(self, request):
        from .models import plugin_loader
        return render(request, 'request_available.html', {
            'is_admin': check_admin(request.user),
            'request_type_list': [
                rt for rt in plugin_loader.request_types if rt.request_visible
            ]
        })


class RequestDetailView(LoginRequiredMixin, View):
    """Allows review of requests by admins as well as the user who
    submitted the request. The submitting user may confirm their
    request, publishing it to the list so admins can review it, or
    cancel it, deleting the request components. Admins may Approve,
    Deny, or mark the request as approved Pending review. They may also
    make minor alterations to fix typos. Interactions with the request
    after it is confirmed will be recorded and reported.
    """
    def get(self, request, pk):
        from .models import plugin_loader
        req = get_object_or_404(Request, pk=pk)
        is_admin = check_admin(request.user)

        # Only an admin or the requester should be able to review a Request
        if req.requester != request.user.username and not is_admin:
            return HttpResponseForbidden()

        if is_admin:
            request_log(req, 'VIEW - {0}'.format(request.user))

        widgets = []
        for model in plugin_loader.request_types:
            widgets.extend(model.objects.filter(request=req))

        return render(request, 'request_detail.html', {
            'req'           : req,
            'widgets'       : widgets,
            'history'       : RequestLogEntry.objects.filter(request=req),
            'email_options' : EmailTemplate.objects.all(),
            'is_admin'      : is_admin,
            'is_requester'  : req.requester == request.user.username,
        })

    def post(self, request, pk):
        req = get_object_or_404(Request, pk=pk)
        is_admin = check_admin(request.user)
        action = request.POST.get('action', '')
        next = request.POST.get('next', 
            req.review_url() if is_admin else reverse('user_requests:conclusion')
        )
        request_log(req, '{0} - {1}'.format(action, request.user))
        
        if req.requester != request.user.username and not is_admin:
            return HttpResponseForbidden() # Only an admin or the requester should view
        if not is_admin and action not in ('CONFIRM', 'CANCEL'):
            return HttpResponseForbidden() # The requester can only confirm or cancel
        
        if action == 'CONFIRM':
            req.on_confirmed()
        elif action == 'CANCELLED':
            req.status = action
            req.save()
        elif action == 'UPDATE':
            req.requester = request.POST.get('requester', req.requester)
            req.description = request.POST.get('description', req.description)
            req.notification_id = int(request.POST.get('notification', req.notification))
            req.save()
        elif action == 'PENDING':
            req.on_pending()
        elif action == 'APPROVED':
            req.on_approved()
        elif action == 'DECLINED':
            req.status = action
            req.save()
        elif action == 'PROCESSED':
            req.on_processed()
        
        return redirect(next)


class RequestUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    """Base class for Updating a request. Should be subclassed for each
    type of request and provide the fields that admins can edit after
    the request has been confirmed.
    """
    model = RequestType

    def test_func(self):
        return check_admin(self.request.user)

    def get(self, request):
        return HttpResponseForbidden() # No GET, only POST

    def form_invalid(self, form):
        return HttpResponseBadRequest()

    def form_valid(self, form):
        request_log(form.instance.request, 'MODIFIED - {0}'.format(self.request.user))
        return super(RequestUpdateView, self).form_valid(form)


class RequestListView(LoginRequiredMixin, UserPassesTestMixin, ListView):
    """Displays confirmed requests. Supports pagination and filtering
    based on status and requesting user's name.
    """
    context_object_name = 'requests'
    template_name  = 'request_list.html'
    paginate_by = 25

    def test_func(self):
        return check_admin(self.request.user)

    def get_queryset(self):
        filters = {'confirmed': True}
        for field in ['status', 'requester', 'description__icontains']:
            if field in self.request.GET and self.request.GET.get(field,''):
                filters[field] = self.request.GET[field]
        return Request.objects.filter(**filters).order_by('-submitted')


class RequestConclusionView(TemplateView):
    template_name = 'request_conclusion.html'
