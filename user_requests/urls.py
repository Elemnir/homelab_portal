from django.urls    import path

from .models    import plugin_loader
from .views     import (AvailableRequestsView, RequestConclusionView,
                        RequestDetailView, RequestListView)

app_name = "user_requests"

urlpatterns = [
    path('', AvailableRequestsView.as_view(), name='available'),
    path('list/', RequestListView.as_view(), name='list'),
    path('view/<int:pk>/', RequestDetailView.as_view(), name='review'),
    path('conclusion/', RequestConclusionView.as_view(), name='conclusion'),
] + plugin_loader.urls # Add the URLs for the plugins
