import datetime

from django.contrib             import messages
from django.http                import HttpResponse, HttpResponseForbidden
from django.shortcuts           import redirect, render
from django.urls                import reverse_lazy
from django.utils.timezone      import now
from django.views               import View
from django.views.generic.edit  import FormView

from admin_toolbelt.contrib.login_records.models import LoginRecord

from openacct.models    import Project, StorageCommitment, User
from userportal.utils   import get_ldap_client

from .forms     import ChangeEmailForm, ChangeShellForm, SetDefaultProjectForm

class Profile(View):
    def get(self, request, username=None):
        if username and not request.user.is_staff:
            return HttpResponseForbidden()

        user = User.objects.get(name=(username if username else request.user.username))
        with get_ldap_client() as lc:
            ldap = lc.search_user(user.name)

        return render(request, "registration/profile.html", {
            "user": user,
            "ldap": ldap,
            "projects": user.projects.all(),
            "logins": LoginRecord.objects.order_by('-when').filter(
                user=user.name, when__gte=now()-datetime.timedelta(days=90))[:20],
            "shell_form": ChangeShellForm(initial={'shell': ldap['loginShell'][0]}),
            "email_form": ChangeEmailForm(initial={'email': ldap['mail'][0]}),
        })

    def post(self, request, username=None):
        if username and not request.user.is_staff:
            return HttpResponseForbidden()

        user = User.objects.get(name=(username if username else request.user.username))
        action = request.POST.get('action', '')
        if action == 'change_shell':
            form = ChangeShellForm(request.POST)
            if form.is_valid():
                form.process(user.name)
                messages.info(request, 'New logins will use the new shell.')
            else:
                messages.error(request, 'Invalid value for shell')
    
        elif action == 'update_email':
            form = ChangeEmailForm(request.POST)
            if form.is_valid():
                form.process(user.name)
                messages.info(request, 'Email address updated.')
            else:
                messages.error(request, 'Invalid value for email address')
        
        elif action == 'set_default_project':
            form = SetDefaultProjectForm(request.POST, instance=user)
            if form.is_valid():
                form.save()
                messages.info(request, 'Default project updated.')
            else:
                messages.error(request, 'Invalid value for default project')

        return redirect(request.path)
                
