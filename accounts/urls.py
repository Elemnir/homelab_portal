from django.urls            import include, path

from .views import Profile

urlpatterns = [
    path("", include('django.contrib.auth.urls')),
    path("profile/",  Profile.as_view(),  name='accounts-profile'),
    path("profile/<username>/",  Profile.as_view(),  name='accounts-profile-byname'),
]
