from django                     import forms
from django.conf                import settings
from django.contrib.auth.models import User
from django.contrib.auth.forms  import UserCreationForm

from openacct.models    import User as OAUser, Project
from userportal.utils   import BootstrapModelForm, get_ldap_client


ACCOUNTS_SHELL_OPTIONS = getattr(settings, 'ACCOUNTS_SHELL_OPTIONS', ['/bin/bash', '/bin/sh'])


class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")
    email = forms.EmailField(max_length=30, required=True)


class ChangeEmailForm(forms.Form):
    email = forms.CharField(
        widget=forms.TextInput(attrs={'class':'form-control'}),
    )

    def process(self, username):
        with get_ldap_client() as lc:
            lc.modify_user_attr(
                username, 'mail', lc.prepare_attribute(self.cleaned_data.get('email'))
            )


class ChangeShellForm(forms.Form):
    shell = forms.ChoiceField(
        choices=[ (i, i) for i in ACCOUNTS_SHELL_OPTIONS ],
        widget=forms.Select(attrs={'class':'form-control'}),
    )

    def process(self, username):
        with get_ldap_client() as lc:
            lc.modify_user_attr(
                username, 'loginShell', lc.prepare_attribute(self.cleaned_data.get('shell'))
            )


class SetDefaultProjectForm(forms.ModelForm):
    class Meta:
        model = OAUser
        fields = ['default_project']
    
    def __init__(self, *args, **kwargs):
        if 'instance' not in kwargs or not isinstance(kwargs['instance'], self._meta.model):
            raise ValueError("This form can only be used on an existing instance")
        super().__init__(*args, **kwargs)
        self.fields['default_project'] = forms.ModelChoiceField(
            queryset=self.instance.projects.all(),
            widget=forms.Select(attrs={'class':'form-control'})
        )

    def save(self, commit=True):
        user = super().save(commit=commit)
        with get_ldap_client() as lc:
            result = lc.search_group(user.default_project.ldap_group)
            if result != None:
                lc.modify_user_attr(
                    user.name, 'gidNumber', lc.prepare_attribute(result['gidNumber'][0])
                )
        return user

