import random

from django.db  import models


def generate_token():
    return ''.join(
       [ random.choice('abcdefghijfklmnopqrstuvwxyz0123456789') for i in range(24) ]
    )
 

class OtpAccessToken(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    used    = models.DateTimeField(null=True, blank=True)
    user    = models.CharField(max_length=32)
    token   = models.CharField(max_length=32, unique=True, default=generate_token)

    def __str__(self):
        return '{} - {}'.format(self.user, self.created)

