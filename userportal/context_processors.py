from django.urls import reverse

def nav_menu(request):
    base_tabs = [
        (
            { 'name': 'Make Requests', 'url': '' },
            [
                { 'name': 'Request to be added to a project', 'url': reverse('user_requests:project_access_info') },
                { 'name': 'Request a new project', 'url': reverse('user_requests:new_project_info') },
                { 'name': 'Request a virtual machine', 'url': reverse('user_requests:new_vm_info') },
            ]
        ),
    ]
    admin_tabs = [
        (
            { 'name': 'View Tickets' },
            [
                { 'name': 'View Requests', 'url': reverse('user_requests:list') },
            ]
        ),
    ]
    
    if request.user.is_authenticated and request.user.is_staff:
        return { 'nav_menu': base_tabs + admin_tabs }
    else:
        return { 'nav_menu': base_tabs }

