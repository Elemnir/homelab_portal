from base64 import encodebytes
import os, random, hashlib, sys

from admin_toolbelt.ldap_client import LdapClient
from django.conf                import settings
from django.contrib.auth.models import User
from django.forms               import ModelForm
from django.forms.utils         import ErrorList
from django.utils.html          import escape
from django.utils.safestring    import mark_safe

class BootstrapErrorList(ErrorList):
    def __str__(self):
        return self.as_alert()

    @mark_safe
    def as_alert(self):
        return ('<div class="alert alert-danger" role="alert">{}</div>'.format(
            ''.join(['<p class="error">{}</p>'.format(escape(e)) for e in self])
        ) if self else '')
    
    @mark_safe
    def as_p(self):
        return ''.join(['<p class="form-text">{}</p>'.format(escape(e)) for e in self]) if self else ''
        

class BootstrapModelForm(ModelForm):
    def __init__(self, *args, error_class=BootstrapErrorList, **kwargs):
        super().__init__(*args, error_class=error_class, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'


def get_user_email(username):
    with get_ldap_client() as lc:
        user = lc.search_user(username)
        if 'mail' in user:
            return user['mail'][0]
        return User.objects.get(username=username).email


def get_ldap_client():
    return LdapClient(settings.AUTH_LDAP_BASE, 
        host=settings.AUTH_LDAP_SERVER_URI, 
        user=settings.AUTH_LDAP_BIND_DN, 
        cred=settings.AUTH_LDAP_BIND_PASSWORD, 
        start_tls=settings.AUTH_LDAP_START_TLS,
    )
